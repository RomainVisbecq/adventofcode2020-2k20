package Utils;

import java.io.FileNotFoundException;

import day.Day01;
import day.Day10;
import day.Day02;
import day.Day03;
import day.Day04;
import day.Day05;
import day.Day06;
import day.Day07;
import day.Day08;
import day.Day09;
import dayOptim.Day1Optim;
import dayOptim.Day2Optim;
import dayOptim.Day3Optim;
import dayOptim.Day7Optim;

public class Bench {

	public static void main(String[] args) throws FileNotFoundException {
		long totaloutPutNonOptim = 0;
		long totalOutPutOptim = 0;
		
		//start
        long lStartTime = System.nanoTime();
        
//		Day1.main(args);
//        Day2.main(args);
//        Day3.main(args);
//        Day4.main(args);
//        Day5.main(args);
//        Day6.main(args);
//        Day7.main(args);
//        Day8.main(args);
        Day10.main(args);

		//end
        long lEndTime = System.nanoTime();
        
		//start
        long lStartTimeO = System.nanoTime();
        
//        Day1Optim.main(args);
//        Day2Optim.main(args);
//        Day3Optim.main(args);
//        Day4.main(args);
//        Day5.main(args);
//        Day6.main(args);
//        Day7Optim.main(args);
//        Day8.main(args);
          Day10.main(args);

		//end
        long lEndTimeO = System.nanoTime();
        
        totaloutPutNonOptim += (lEndTime - lStartTime)/1000000;
        totalOutPutOptim = (lEndTimeO - lStartTimeO)/1000000;
        
        System.out.println("Non Optim : " + totaloutPutNonOptim);
        System.out.println("Optim: " + totalOutPutOptim);
	}

}
