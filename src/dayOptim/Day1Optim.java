package dayOptim;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Day1Optim {

	public static void main(String[] args) throws FileNotFoundException {
		
		Scanner sc = new Scanner(new File("src/sources/day1.txt"));
		ArrayList<Integer> totalFrais = new ArrayList<Integer>();
		
		while(sc.hasNextLine()){
			totalFrais.add(Integer.valueOf(sc.nextLine()));
		}
		
		System.out.println(partOne(totalFrais));
		System.out.println(partTwo(totalFrais));
	}
	
	static int partOne(ArrayList<Integer> totalFrais){
		
		for(Integer frais : totalFrais){
			for(Integer frais2 : totalFrais){
				if(frais+frais2 == 2020){
					return frais*frais2;
				}
			}
		}
		return 0;
	}
	
	static int partTwo(ArrayList<Integer> totalFrais){
		
		for(Integer frais : totalFrais){
			for(Integer frais2 : totalFrais){
				for(Integer frais3 : totalFrais){
					if(frais+frais2+frais3 == 2020){
						return frais*frais2*frais3;
					}
				}
			}
		}
		return 0;
	}

}
