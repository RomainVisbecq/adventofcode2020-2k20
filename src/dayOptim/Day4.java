package dayOptim;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Scanner;

public class Day4 {

	public static void main(String[] args) throws FileNotFoundException {
		
		Scanner sc = new Scanner(new File("src/sources/day4.txt"));
		ArrayList<String> lines = new ArrayList<String>();
		String insert ="";
		
		while(sc.hasNextLine()){
			String lig =sc.nextLine();
			if(lig.length() ==0){
				lines.add(insert);
				insert ="";
			}else{
				insert += " "+lig;
			}
		}
		
		lines.add(insert);
		
		System.out.println(partOne(lines));
		System.out.println(partTwo(lines));
	}
	
	static int partOne(ArrayList<String> lines){

		int valid = 0;
		for(String passport : lines){
			if(passport.contains("byr:") && passport.contains("iyr:") && passport.contains("eyr:") && passport.contains("hgt:") && 
					passport.contains("hcl:") && passport.contains("ecl:") && passport.contains("pid:")){
				valid++;
			}
		}
		return valid;
		
	}

	static int partTwo(ArrayList<String> lines){
		int valid = 0;
		for(String passport : lines){
			if(passport.contains("byr:") && passport.contains("iyr:") && passport.contains("eyr:") && passport.contains("hgt:") && 
					passport.contains("hcl:") && passport.contains("ecl:") && passport.contains("pid:")){
				String[] categorie = passport.split(" ");
				int catValid = 0;
				for(String cat : categorie){
					String[] choisir = cat.split(":");
					String key = choisir[0];
					String value = "";
					if(choisir.length>1){
						value = choisir[1];
					}
					
					if(key.length()>0 && value.length()>0){

						if(key.equals("byr") && value.matches("\\d+")){
							//	byr(Année de naissance) - quatre chiffres; au moins 1920 et au plus 2002.
							long chiffre = Integer.parseInt(choisir[1]);
							if(chiffre>=1920 && chiffre <= 2002){
								catValid++;
							}
						}else if(key.equals("iyr") && value.matches("\\d+")){
							// 	iyr(Année d'émission) - quatre chiffres; au moins 2010 et au plus 2020.
							long chiffre = Integer.parseInt(choisir[1]);
							if(chiffre>=2010 && chiffre <= 2020){
								catValid++;
							}
						}else if(key.equals("eyr") && value.matches("\\d+")){
							// 	eyr(Année d'expiration) - quatre chiffres; au moins 2020 et au plus 2030.
							long chiffre = Integer.parseInt(value);
							if(chiffre>=2020 && chiffre <= 2030){
								catValid++;
							}
						}else if(key.equals("hgt")){
							/*
							 	hgt(Hauteur) - un nombre suivi de l'un cmou l' autre ou in:
								Si cm, le nombre doit être au moins 150et au plus 193.
								Si in, le nombre doit être au moins 59et au plus 76.
							 * */
							if(value.substring(value.length()-2).equals("cm") && value.substring(0,value.length()-2).matches("\\d+")){
								long chiffre = Integer.parseInt(value.substring(0,value.length()-2));
								if(chiffre>=150 && chiffre <= 193){
									catValid++;
								}		
							}else if(value.substring(value.length()-2).equals("in") && value.substring(0,value.length()-2).matches("\\d+")){
								long chiffre = Integer.parseInt(value.substring(0,value.length()-2));
								if(chiffre>=59 && chiffre <= 76){
									catValid++;
								}
							}

						}else if(key.equals("hcl")){
							//	hcl(Couleur des cheveux) - a #suivi d'exactement six caractères 0- 9ou a- f
							if(value.charAt(0) == '#' && value.substring(1,value.length()).length() == 6 && value.substring(1, value.length()).matches("^[a-fA-F0-9]+")){
								catValid++;
							}
						}else if(key.equals("ecl")){
							//	ecl(Couleur des yeux) - exactement un: amb blu brn gry grn hzl oth.
							if(value.equals("amb") || value.equals("blu") || value.equals("brn") || value.equals("gry") || value.equals("grn") || value.equals("hzl") || value.equals("oth")){
								catValid++;
							}
						}else if(key.equals("pid")){
							// pid (ID de passeport) - un numéro à neuf chiffres, y compris les zéros non significatifs.
							if(value.length() == 9 && value.matches("\\d+")){
								catValid++;
							}
						}else{
							
						}
					}
				}
				if(catValid==7){
					valid++;
				}
			}
		}
		return valid;
		}
}
