package dayOptim;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Day5 {

	public static void main(String[] args) throws FileNotFoundException {
        
		Scanner sc = new Scanner(new File("src/sources/day5.txt"));
		ArrayList<String> lines = new ArrayList<String>();
		
		while(sc.hasNextLine()){
			String insert =sc.nextLine();
			lines.add(insert);
		}
		
		System.out.println(partOne(lines));
		System.out.println(partTwo(lines));

	}
	
	static int partOne(ArrayList<String> lines){

		int max = 0;
		
		for(String siege : lines){
			int rowI = 0;
			int colI = 0;
			int rowS = 7;
			int colS = 127;
			
			int row = 7;
			int col = 127;
			int r=0;
						
			while(r<siege.length()){
				char lettre = siege.charAt(r);
				if(lettre == 'F'){
					colS -= Math.ceil((double)(colS-colI)/2);
					col=colS;
				}else if(lettre == 'B'){
					colI += Math.ceil((double)(colS-colI)/2);
					col=colI;
				}
				if(lettre == 'L'){
					rowS -= Math.ceil((double)(rowS-rowI)/2);
					row=rowS;
				}else if(lettre == 'R'){
					rowI += Math.ceil((double)(rowS-rowI)/2);
					row=rowI;
				}

				r++;
			}
			
			int result = col*8+row;
			if(result > max){
				max = result;
			}
		}
		
		return max;
		
	}

	static int partTwo(ArrayList<String> lines){

		int[] totalSiege = new int[lines.size()];
		
		for(int i=0;i<lines.size();i++){
			int rowI = 0;
			int colI = 0;
			int rowS = 7;
			int colS = 127;
			int row = 7;
			int col = 127;
			int r=0;
						
			while(r<lines.get(i).length()){
				char lettre = lines.get(i).charAt(r);
				if(lettre == 'F'){
					colS -= Math.ceil((double)(colS-colI)/2);
					col=colS;
				}else if(lettre == 'B'){
					colI += Math.ceil((double)(colS-colI)/2);
					col=colI;
				}
				if(lettre == 'L'){
					rowS -= Math.ceil((double)(rowS-rowI)/2);
					row=rowS;
				}else if(lettre == 'R'){
					rowI += Math.ceil((double)(rowS-rowI)/2);
					row=rowI;
				}

				r++;
			}
			
			int result = col*8+row;
			totalSiege[i]=result;
		}
		Arrays.sort(totalSiege);
		
		for(int i=totalSiege[0];i<=totalSiege[totalSiege.length-1];i++){
			for(int j=1;j<totalSiege.length-1;j++){
				if(totalSiege[j] == i-1 && totalSiege[j+1] == i+1){
					return i;
				}
			}

		}
		
		return 0;
		
	}
}
