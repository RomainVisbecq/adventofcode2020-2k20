package day;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Scanner;

public class Day03 {

	public static void main(String[] args) throws FileNotFoundException {
		
		Scanner sc = new Scanner(new File("src/sources/day03.txt"));
		ArrayList<String> lines = new ArrayList<String>();
		
		while(sc.hasNextLine()){
			lines.add(sc.nextLine());
		}
		
		System.out.println(partOne(lines));
		System.out.println(partTwo(lines));

	}
	
	static long partOne(ArrayList<String> lines){
		int sumArbre = 0;
		int curs = 0;
		
		for(String ligne : lines){
			if(ligne.charAt(curs%ligne.length())=='#'){
				sumArbre++;
			}
			curs+=3;
		}		
		
		return sumArbre;
	}
	
	static BigDecimal partTwo(ArrayList<String> lines){
		int sumArbre1 = 0;
		int sumArbre2 = 0;
		int sumArbre3 = 0;
		int sumArbre4 = 0;
		int sumArbre5 = 0;

		int curs1 = 0;
		int curs2 = 0;
		int curs3 = 0;
		int curs4 = 0;
		int curs5 = 0;
		
		for(String ligne : lines){
			if(ligne.charAt(curs1%ligne.length())=='#'){
				sumArbre1++;
			}
			curs1+=1;
			if(ligne.charAt(curs2%ligne.length())=='#'){
				sumArbre2++;
			}
			curs2+=3;
			if(ligne.charAt(curs3%ligne.length())=='#'){
				sumArbre3++;
			}
			curs3+=5;
			if(ligne.charAt(curs4%ligne.length())=='#'){
				sumArbre4++;
			}
			curs4+=7;
		}
		
		for(int i=0;i<lines.size();i=i+2){
			if(lines.get(i).charAt(curs5%lines.get(i).length())=='#'){
				sumArbre5++;
			}
			curs5+=1;
		}
		
		return BigDecimal.valueOf(sumArbre1*sumArbre2*sumArbre3*sumArbre4).multiply(BigDecimal.valueOf(sumArbre5));

	}
}
