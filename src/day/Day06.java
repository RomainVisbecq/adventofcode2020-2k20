package day;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class Day06 {

	public static void main(String[] args) throws FileNotFoundException {
		
		Scanner sc = new Scanner(new File("src/sources/day06.txt"));
		ArrayList<String> lines = new ArrayList<String>();
		String insert ="";
	
		while(sc.hasNextLine()){
			String lig =sc.nextLine();
			if(lig.length() ==0){
				lines.add(insert);
				insert ="";
			}else{
				insert += ";"+lig;
			}
		}
		
		lines.add(insert);
		
		System.out.println(partOne(lines));
		System.out.println(partTwo(lines));

	}
	
	static int partOne(ArrayList<String> lines){
		int nbYes = 0;
		
		for(String groupe : lines){
			Set<String> countAnswer = new HashSet<String>();
			for(String answer : groupe.split(";")) {
				for(char yes : answer.toCharArray())
				countAnswer.add(String.valueOf(yes));
			}
			nbYes += countAnswer.size();
			
		}
		
		return nbYes;
		
	}

	static int partTwo(ArrayList<String> lines){
		int nbYes = 0;
		
		for(String groupe : lines){
			Set<String> countAnswer = new HashSet<String>();
			Map<String,Integer> result = new HashMap<String,Integer>();

			for(String answer : groupe.split(";")) {
				for(char yes : answer.toCharArray())
				if(result.get(String.valueOf(yes)) == null) {
					result.put(String.valueOf(yes), 1);
				}else {
					Integer value = result.get(String.valueOf(yes));
					result.remove(String.valueOf(yes));
					result.put(String.valueOf(yes), value + 1);
				}

			}
						
			for (Map.Entry<String, Integer> entry : result.entrySet()) {
				if(entry.getValue().intValue() == groupe.split(";").length-1) {
					nbYes++;
				}
			}
			
		}
		
		return nbYes;
		
	}
}
