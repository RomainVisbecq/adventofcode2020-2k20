package day;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Day09 {
	
	private static int prec = 25;
	
	public static void main(String[] args) throws FileNotFoundException {
				
		Scanner sc = new Scanner(new File("src/sources/day09.txt"));
		ArrayList<Long> def = new ArrayList<Long>();
	
		while(sc.hasNextLine()){
			String insert =sc.nextLine();
			def.add(Long.valueOf(insert));
		}
		
		long partOne;
		System.out.println(partOne = partOne(def));
		System.out.println(partTwo(def,partOne));
		
	}
	
	public static long partOne(ArrayList<Long> def) {
		
		for(int i=prec;i<def.size();i++) {
			boolean trouve = false;
			for(int j=i-prec;j<i &&!trouve;j++) {
				for(int k=j+1;k<i;k++) {
					if(def.get(j).longValue() != def.get(k).longValue() && def.get(j).longValue()+def.get(k).longValue() == def.get(i).longValue()) {
						trouve = true;
						break;
					}
				}
			}
			if(!trouve) {
				return def.get(i);
			}
		}
		return 0;
	
	}
	
	public static long partTwo(ArrayList<Long> def,long intru) {
			
		for(int i=0;i<def.size();i++) {
			long result = 0;
			long min = Integer.MAX_VALUE;
			long max = 0;
			int j = i;
			while(result <intru) {
				long nombre = def.get(j).longValue();
				if(nombre>max) {
					max = nombre;
				}
				if(nombre<min) {
					min = nombre;
				}
				result += nombre;
				j++;
			}
			if(result == intru) {
				return min+max;
			}
		}
		return 0;
	
	}
}

