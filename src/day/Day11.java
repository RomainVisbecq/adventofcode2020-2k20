package day;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Day11 {
	
	public static void main(String[] args) throws FileNotFoundException {
				
		Scanner sc = new Scanner(new File("src/sources/day11.txt"));
		ArrayList<String> def = new ArrayList<String>();
		
		while(sc.hasNextLine()){
			String insert =sc.nextLine();
			def.add(insert);
		}
		
		int[][] grille = new int[def.size()][def.get(0).length()];
		
		for(int i=0;i<grille.length;i++) {
			for(int j=0;j<grille[i].length;j++) {
				if(def.get(i).charAt(j) == 'L') {
					grille[i][j] = 1;
				}else if(def.get(i).charAt(j) == '#') {
					grille[i][j] = 2;
				}else {
					grille[i][j] = 0;
	
				}
			}
		}
		
		
		
		System.out.println(partOne(grille));
		System.out.println(partTwo(grille));
	
		
	}
	
	public static int partOne(int[][] grille) {
		
		int[][] grilleRef = new int[grille.length][grille[0].length];
		for(int i=0;i< grille.length;i++) {
			for(int j=0;j< grille[i].length;j++) {
				grilleRef[i][j] = grille[i][j];
			}
		}
	
		int retour = 0;
		boolean nonEgal = true;
		
		while(nonEgal) {
			retour = 0;
			for(int i=0;i<grilleRef.length;i++) {
				for(int j=0;j<grilleRef[i].length;j++) {
					if(grilleRef[i][j] == 1) {
						boolean ok = false;
						if(i == 0) {
							if(j == 0) {
								if(grilleRef[i][j+1] != 2 && grilleRef[i+1][j+1] !=2 && grilleRef[i+1][j] != 2){
									ok = true;
								}
							}else if(j == grilleRef[i].length-1 ) {
								if(grilleRef[i+1][j] != 2 && grilleRef[i][j-1] != 2 &&grilleRef[i+1][j-1] != 2) {
									ok = true;
								}
							}else {
								if(grilleRef[i][j+1] != 2 &&grilleRef[i+1][j+1] != 2 &&grilleRef[i+1][j] != 2 &&grilleRef[i][j-1] != 2 && grilleRef[i+1][j-1] != 2) {
									ok = true;
								}
							}
	
						}else if(i == grilleRef.length-1 ) {
							if(j == 0 ) {
								if(grilleRef[i][j+1] != 2 && grilleRef[i-1][j+1] != 2 && grilleRef[i-1][j] != 2) {
									ok = true;
								}
							}else if(j == grilleRef[i].length-1 ) {
								if(grilleRef[i][j-1] != 2 && grilleRef[i-1][j-1] != 2 && grilleRef[i-1][j] != 2) {
									ok = true;
								}
							}else {
								if(grilleRef[i][j-1] != 2 && grilleRef[i-1][j-1] != 2 && grilleRef[i-1][j] != 2 && grilleRef[i][j+1] != 2 && grilleRef[i-1][j+1] != 2) {
									ok = true;
								}
							}
						}else {
							if(j==0) {
								if(grilleRef[i-1][j] != 2 && grilleRef[i-1][j+1] != 2 && grilleRef[i][j+1] != 2 && grilleRef[i+1][j] != 2 && grilleRef[i+1][j+1] != 2) {
									ok = true;
								}
							}else if(j == grilleRef[i].length-1 ) {
								if(grilleRef[i-1][j-1] != 2 && grilleRef[i-1][j] != 2 && grilleRef[i][j-1] != 2 && grilleRef[i+1][j-1] != 2 && grilleRef[i+1][j] != 2) {
									ok = true;
								}
							}else {
								if(grilleRef[i-1][j-1] != 2 && grilleRef[i-1][j] != 2 && grilleRef[i-1][j+1] != 2 && grilleRef[i][j-1] != 2 && grilleRef[i][j+1] != 2 && 
										grilleRef[i+1][j-1] != 2 && grilleRef[i+1][j] != 2 && grilleRef[i+1][j+1] != 2) {
									ok = true;
								}
							}
						}
						if(ok) {
							grille[i][j] = 2;
						}
					}else if(grilleRef[i][j] == 2) {
						int okInt = 0;
						boolean ok=false;
						if(i == 0) {
							if(j==0) {
								if(grilleRef[i][j+1] == 2) okInt+=2;
								if(grilleRef[i+1][j+1] ==2) okInt+=2;
								if(grilleRef[i+1][j] ==2) okInt+=2;
								if(okInt>= 8) {
									ok = true;
								}
							}else if(j == grilleRef[i].length-1 ) {
								if(grilleRef[i+1][j] == 2) okInt+=2;
								if(grilleRef[i][j-1] == 2) okInt+=2;
								if(grilleRef[i+1][j-1] == 2) okInt+=2;
								if(okInt>= 8) {
									ok = true;
								}
							}else {
								if(grilleRef[i][j+1] == 2) okInt+=2;
								if(grilleRef[i+1][j+1] == 2) okInt+=2;
								if(grilleRef[i+1][j] == 2) okInt+=2;
								if(grilleRef[i][j-1] == 2) okInt+=2;
								if(grilleRef[i+1][j-1] == 2) okInt+=2;
								if(okInt>= 8) {
									ok = true;
								}
							}
	
						}else if(i == grilleRef.length-1 ) {
							if(j == 0 ) {
								if(grilleRef[i][j+1]==2) okInt+=2;
								if(grilleRef[i-1][j+1]==2) okInt+=2;
								if(grilleRef[i-1][j] ==2) okInt+=2;
								if(okInt>= 8) {
									ok = true;
								}
							}else if(j == grilleRef[i].length-1 ) {
								if(grilleRef[i][j-1]==2) okInt+=2;
								if(grilleRef[i-1][j-1]==2) okInt+=2;
								if(grilleRef[i-1][j]==2) okInt+=2;
								if(okInt>= 8) {
									ok = true;
								}
							}else {
								if(grilleRef[i][j-1]==2) okInt+=2;
								if(grilleRef[i-1][j-1]==2) okInt+=2;
								if(grilleRef[i-1][j]==2) okInt+=2;
								if(grilleRef[i][j+1]==2) okInt+=2;
								if(grilleRef[i-1][j+1]==2) okInt+=2;
								if(okInt>= 8) {
									ok = true;
								}
							}
						}else {
							if(j==0) {
								if(grilleRef[i-1][j] ==2) okInt+=2;
								if(grilleRef[i-1][j+1] ==2) okInt+=2;
								if(grilleRef[i][j+1] ==2) okInt+=2;
								if(grilleRef[i+1][j] ==2) okInt+=2;
								if(grilleRef[i+1][j+1]==2) okInt+=2;
								if(okInt>= 8) {
									ok = true;
								}
							}else if(j == grilleRef[i].length-1 ) {
								if(grilleRef[i-1][j-1] ==2) okInt+=2;
								if(grilleRef[i-1][j] ==2) okInt+=2;
								if(grilleRef[i][j-1] ==2) okInt+=2;
								if(grilleRef[i+1][j-1] ==2) okInt+=2;
								if(grilleRef[i+1][j]==2) okInt+=2;
								if(okInt>= 8) {
									ok = true;
								}
							}else {
								if(grilleRef[i-1][j-1] ==2) okInt+=2;
								if(grilleRef[i-1][j] ==2) okInt+=2;
								if(grilleRef[i-1][j+1] ==2) okInt+=2;
								if(grilleRef[i][j-1] ==2) okInt+=2;
								if(grilleRef[i][j+1] ==2) okInt+=2;
								if(grilleRef[i+1][j-1] ==2) okInt+=2;
								if(grilleRef[i+1][j] ==2) okInt+=2;
								if(grilleRef[i+1][j+1]==2) okInt+=2;
								if(okInt>= 8) {
									ok = true;
								}
							}
						}
						if(ok) {
							grille[i][j] = 1;
						}
					}
				}
			}
			
			boolean egal = true;
		    for (int i = 0; i < grilleRef.length; i++) {
				for(int j=0;j< grilleRef[i].length;j++) {
			        if(grilleRef[i][j] != grille[i][j]) {
			        	egal = false;
			        }
		        }
		    }
		    
		    if(egal) {
		    	nonEgal = false;
		    }
		    
		    
		    
			for(int i=0;i< grille.length;i++) {
				for(int j=0;j< grille[i].length;j++) {
					grilleRef[i][j] = grille[i][j];
					if(grille[i][j]== 2) {
						retour++;
					}
				}
			}
			
		}
		
		return retour;
	}


	public static int partTwo(int[][] grille) {
		
		int[][] grilleRef = new int[grille.length][grille[0].length];
		for(int i=0;i< grille.length;i++) {
			for(int j=0;j< grille[i].length;j++) {
				grilleRef[i][j] = grille[i][j];
			}
		}
	
		int retour = 0;
		boolean nonEgal = true;
		
		while(nonEgal) {
			retour = 0;
			for(int i=0;i<grilleRef.length;i++) {
				for(int j=0;j<grilleRef[i].length;j++) {
					if(grilleRef[i][j] == 1) {
						boolean ok = false;
						int incrI=0;
						int incrJ=0;
						if(i == 0) {
							if(j == 0) {
								if(grilleRef[i][j+1] != 2 && grilleRef[i+1][j+1] !=2 && grilleRef[i+1][j] != 2){
									ok = true;
									if(grilleRef[i][j+1] == 0) {
										incrJ=j+1;
										while(incrJ<grilleRef[i].length && (grilleRef[i][incrJ] == 0 || grilleRef[i][incrJ] == 2) && ok) {
											if(grilleRef[i][incrJ] == 2) {
												ok= false;
											}
											incrJ++;
										}
									}
									if(grilleRef[i+1][j+1] == 0) {
										incrI=i+1;
										incrJ=j+1;
										while(incrI < grilleRef.length && incrJ<grilleRef[i].length && (grilleRef[incrI][incrJ] == 0 || grilleRef[incrI][incrJ] == 2) && ok) {
											if(grilleRef[incrI][incrJ] == 2) {
												ok= false;
											}
											incrI++;
											incrJ++;
										}
									}
									if(grilleRef[i+1][j] == 0) {
										incrI=i+1;
										while(incrI < grilleRef.length && (grilleRef[incrI][j] == 0 || grilleRef[incrI][j] == 2) && ok) {
											if(grilleRef[incrI][j] == 2) {
												ok= false;
											}
											incrI++;
										}
									}					
								}
							}else if(j == grilleRef[i].length-1 ) {
								if(grilleRef[i+1][j] != 2 && grilleRef[i][j-1] != 2 && grilleRef[i+1][j-1] != 2) {
									ok = true;
									if(grilleRef[i+1][j] == 0) {
										incrI=i+1;
										while(incrI < grilleRef.length && (grilleRef[incrI][j] == 0 || grilleRef[incrI][j] == 2) && ok) {
											if(grilleRef[incrI][j] == 2) {
												ok= false;
											}
											incrI++;
										}
									}
									if(grilleRef[i][j-1] == 0) {
										incrJ=j-1;
										while(incrJ >= 0 && (grilleRef[i][incrJ] == 0 || grilleRef[i][incrJ] == 2) && ok) {
											if(grilleRef[i][incrJ] == 2) {
												ok= false;
											}
											incrJ--;
										}
									}
									if(grilleRef[i+1][j-1] == 0) {
										incrI=i+1;
										incrJ=j-1;
										while(incrI < grilleRef.length && incrJ>=0 && (grilleRef[incrI][incrJ] == 0 || grilleRef[incrI][incrJ] == 2) && ok) {
											if(grilleRef[incrI][incrJ] == 2) {
												ok= false;
											}
											incrI++;
											incrJ--;
										}
									}						
								}
							}else {
								if(grilleRef[i][j+1] != 2 && grilleRef[i+1][j+1] != 2 && grilleRef[i+1][j] != 2 && grilleRef[i][j-1] != 2 && grilleRef[i+1][j-1] != 2) {
									ok = true;
									if(grilleRef[i][j+1] == 0) {
										incrJ=j+1;
										while(incrJ<grille[i].length && (grilleRef[i][incrJ] == 0 || grilleRef[i][incrJ] == 2) && ok) {
											if(grilleRef[i][incrJ] == 2) {
												ok= false;
											}
											incrJ++;
										}
									}
									if(grilleRef[i+1][j+1] == 0) {
										incrI=i+1;
										incrJ=j+1;
										while(incrI < grilleRef.length && incrJ<grilleRef[i].length && (grilleRef[incrI][incrJ] == 0 || grilleRef[incrI][incrJ] == 2) && ok) {
											if(grilleRef[incrI][incrJ] == 2) {
												ok= false;
											}
											incrI++;
											incrJ++;
										}
									}
									if(grilleRef[i+1][j] == 0) {
										incrI=i+1;
										while(incrI < grilleRef.length && (grilleRef[incrI][j] == 0 || grilleRef[incrI][j] == 2) && ok) {
											if(grilleRef[incrI][j] == 2) {
												ok= false;
											}
											incrI++;
										}
									}
									if(grilleRef[i][j-1] == 0) {
										incrJ=j-1;
										while(incrJ >= 0 && (grilleRef[i][incrJ] == 0 || grilleRef[i][incrJ] == 2) && ok) {
											if(grilleRef[i][incrJ] == 2) {
												ok= false;
											}
											incrJ--;
										}
									}
									if(grilleRef[i+1][j-1] == 0) {
										incrI=i+1;
										incrJ=j-1;
										while(incrI < grilleRef.length && incrJ >= 0 && (grilleRef[incrI][incrJ] == 0 || grilleRef[incrI][incrJ] == 2) && ok) {
											if(grilleRef[incrI][incrJ] == 2) {
												ok= false;
											}
											incrI++;
											incrJ--;
										}
									}					
								}
							}
						}else if(i == grilleRef.length-1 ) {
							if(j == 0 ) {
								if(grilleRef[i][j+1] != 2 && grilleRef[i-1][j+1] != 2 && grilleRef[i-1][j] != 2) {
									ok = true;
									if(grilleRef[i][j+1] == 0) {
										incrJ=j+1;
										while(incrJ<grilleRef[i].length && (grilleRef[i][incrJ] == 0 || grilleRef[i][incrJ] == 2) && ok) {
											if(grilleRef[i][incrJ] == 2) {
												ok= false;
											}
											incrJ++;
										}
									}
									if(grilleRef[i-1][j+1] == 0) {
										incrI=i-1;
										incrJ=j+1;
										while(incrI >= 0 && incrJ<grilleRef[i].length && (grilleRef[incrI][incrJ] == 0 || grilleRef[incrI][incrJ] == 2) && ok) {
											if(grilleRef[incrI][incrJ] == 2) {
												ok= false;
											}
											incrI--;
											incrJ++;
										}
									}
									if(grilleRef[i-1][j] == 0) {
										incrI=i-1;
										while(incrI >= 0 && (grilleRef[incrI][j]== 0 || grilleRef[incrI][j] == 2) && ok) {
											if(grilleRef[incrI][j] == 2) {
												ok= false;
											}
											incrI--;
										}
									}				
								}
							}else if(j == grilleRef[i].length-1 ) {
								if(grilleRef[i][j-1] != 2 && grilleRef[i-1][j-1] != 2 && grilleRef[i-1][j] != 2) {
									ok = true;
									if(grilleRef[i][j-1] == 0) {
										incrJ=j-1;
										while(incrJ >= 0 && (grilleRef[i][incrJ] == 0 || grilleRef[i][incrJ] == 2) && ok) {
											if(grilleRef[i][incrJ] == 2) {
												ok= false;
											}
											incrJ--;
										}
									}
									if(grilleRef[i-1][j-1] == 0) {
										incrI=i-1;
										incrJ=j-1;
										while(incrI >= 0 && incrJ >= 0 && (grilleRef[incrI][incrJ] == 0 || grilleRef[incrI][incrJ] == 2) && ok) {
											if(grilleRef[incrI][incrJ] == 2) {
												ok= false;
											}
											incrI--;
											incrJ--;
										}
									}
									if(grilleRef[i-1][j] == 0) {
										incrI=i-1;
										while(incrI >= 0 && (grilleRef[incrI][j]== 0 || grilleRef[incrI][j] == 2) && ok) {
											if(grilleRef[incrI][j] == 2) {
												ok= false;
											}
											incrI--;
										}
									}		
								}
							}else {
								if(grilleRef[i][j-1] != 2 && grilleRef[i-1][j-1] != 2 && grilleRef[i-1][j] != 2 && grilleRef[i][j+1] != 2 && grilleRef[i-1][j+1] != 2) {
									ok = true;
									if(grilleRef[i][j-1] == 0) {
										incrJ=j-1;
										while(incrJ >= 0 && (grilleRef[i][incrJ] == 0 || grilleRef[i][incrJ] == 2) && ok) {
											if(grilleRef[i][incrJ] == 2) {
												ok= false;
											}
											incrJ--;
										}
									}
									if(grilleRef[i-1][j-1] == 0) {
										incrI=i-1;
										incrJ=j-1;
										while(incrI >= 0 && incrJ >= 0 && (grilleRef[incrI][incrJ] == 0 || grilleRef[incrI][incrJ] == 2) && ok) {
											if(grilleRef[incrI][incrJ] == 2) {
												ok= false;
											}
											incrI--;
											incrJ--;
										}
									}
									if(grilleRef[i-1][j] == 0) {
										incrI=i-1;
										while(incrI >= 0 && (grilleRef[incrI][j]== 0 || grilleRef[incrI][j] == 2) && ok) {
											if(grilleRef[incrI][j] == 2) {
												ok= false;
											}
											incrI--;
										}
									}
									if(grilleRef[i][j+1] == 0) {
										incrJ=j+1;
										while(incrJ<grilleRef[i].length && (grilleRef[i][incrJ] == 0 || grilleRef[i][incrJ] == 2) && ok) {
											if(grilleRef[i][incrJ] == 2) {
												ok= false;
											}
											incrJ++;
										}
									}
									if(grilleRef[i-1][j+1] == 0) {
										incrI=i-1;
										incrJ=j+1;
										while(incrI >= 0 && incrJ < grilleRef[i].length && (grilleRef[incrI][incrJ] == 0 || grilleRef[incrI][incrJ] == 2) && ok) {
											if(grilleRef[incrI][incrJ] == 2) {
												ok= false;
											}
											incrI--;
											incrJ++;
										}
									}
								}
							}
						}else {
							if(j==0) {
								if(grilleRef[i-1][j] != 2 && grilleRef[i-1][j+1] != 2 && grilleRef[i][j+1] != 2 && grilleRef[i+1][j] != 2 && grilleRef[i+1][j+1] != 2) {
									ok = true;
									if(grilleRef[i-1][j] == 0) {
										incrI=i-1;
										while(incrI >= 0 && (grilleRef[incrI][j]== 0 || grilleRef[incrI][j] == 2) && ok) {
											if(grilleRef[incrI][j] == 2) {
												ok= false;
											}
											incrI--;
										}
									}
									if(grilleRef[i-1][j+1] == 0) {
										incrI=i-1;
										incrJ=j+1;
										while(incrI >= 0 && incrJ<grilleRef[i].length && (grilleRef[incrI][incrJ] == 0 || grilleRef[incrI][incrJ] == 2) && ok) {
											if(grilleRef[incrI][incrJ] == 2) {
												ok= false;
											}
											incrI--;
											incrJ++;
										}
									}
									if(grilleRef[i][j+1] == 0) {
										incrJ=j+1;
										while(incrJ<grilleRef[i].length && (grilleRef[i][incrJ] == 0 || grilleRef[i][incrJ] == 2) && ok) {
											if(grilleRef[i][incrJ] == 2) {
												ok= false;
											}
											incrJ++;
										}
									}
									if(grilleRef[i+1][j] == 0) {
										incrI=i+1;
										while(incrI < grilleRef.length && (grilleRef[incrI][j]== 0 || grilleRef[incrI][j] == 2) && ok) {
											if(grilleRef[incrI][j] == 2) {
												ok= false;
											}
											incrI++;
										}
									}
									if(grilleRef[i+1][j+1] == 0) {
										incrI=i+1;
										incrJ=j+1;
										while(incrI < grilleRef.length && incrJ<grilleRef[i].length && (grilleRef[incrI][incrJ] == 0 || grilleRef[incrI][incrJ] == 2) && ok) {
											if(grilleRef[incrI][incrJ] == 2) {
												ok= false;
											}
											incrI++;
											incrJ++;
										}
									}
								}
							}else if(j == grilleRef[i].length-1 ) {
								if(grilleRef[i-1][j-1] != 2 && grilleRef[i-1][j] != 2 && grilleRef[i][j-1] != 2 && grilleRef[i+1][j-1] != 2 && grilleRef[i+1][j] != 2) {
									ok = true;
									if(grilleRef[i-1][j-1] == 0) {
										incrI=i-1;
										incrJ=j-1;
										while(incrI >= 0 && incrJ >= 0 && (grilleRef[incrI][incrJ] == 0 || grilleRef[incrI][incrJ] == 2) && ok) {
											if(grilleRef[incrI][incrJ] == 2) {
												ok= false;
											}
											incrI--;
											incrJ--;
										}
									}
									if(grilleRef[i-1][j] == 0) {
										incrI=i-1;
										while(incrI >= 0 && (grilleRef[incrI][j]== 0 || grilleRef[incrI][j] == 2) && ok) {
											if(grilleRef[incrI][j] == 2) {
												ok= false;
											}
											incrI--;
										}
									}
									if(grilleRef[i][j-1] == 0) {
										incrJ=j-1;
										while(incrJ >= 0 && (grilleRef[i][incrJ] == 0 || grilleRef[i][incrJ] == 2) && ok) {
											if(grilleRef[i][incrJ] == 2) {
												ok= false;
											}
											incrJ--;
										}
									}
									if(grilleRef[i+1][j-1] == 0) {
										incrI=i+1;
										incrJ=j-1;
										while(incrI < grilleRef.length && incrJ >= 0 && (grilleRef[incrI][incrJ] == 0 || grilleRef[incrI][incrJ] == 2) && ok) {
											if(grilleRef[incrI][incrJ] == 2) {
												ok= false;
											}
											incrI++;
											incrJ--;
										}
									}
									if(grilleRef[i+1][j] == 0) {
										incrI=i+1;
										while(incrI < grilleRef.length && (grilleRef[incrI][j]== 0 || grilleRef[incrI][j] == 2) && ok) {
											if(grilleRef[incrI][j] == 2) {
												ok= false;
											}
											incrI++;
										}
									}
								}
							}else {
								if(grilleRef[i-1][j-1] != 2 && grilleRef[i-1][j] != 2 && grilleRef[i-1][j+1] != 2 && grilleRef[i][j-1] != 2 && grilleRef[i][j+1] != 2 && 
										grilleRef[i+1][j-1] != 2 && grilleRef[i+1][j] != 2 && grilleRef[i+1][j+1] != 2) {
									ok = true;
									if(grilleRef[i-1][j-1] == 0) {
										incrI=i-1;
										incrJ=j-1;
										while(incrI >= 0 && incrJ >= 0 && (grilleRef[incrI][incrJ] == 0 || grilleRef[incrI][incrJ] == 2) && ok) {
											if(grilleRef[incrI][incrJ] == 2) {
												ok= false;
											}
											incrI--;
											incrJ--;
										}
									}
									if(grilleRef[i-1][j] == 0) {
										incrI=i-1;
										while(incrI >= 0 && (grilleRef[incrI][j]== 0 || grilleRef[incrI][j] == 2) && ok) {
											if(grilleRef[incrI][j] == 2) {
												ok= false;
											}
											incrI--;
										}
									}
									if(grilleRef[i][j-1] == 0) {
										incrJ=j-1;
										while(incrJ >= 0 && (grilleRef[i][incrJ] == 0 || grilleRef[i][incrJ] == 2) && ok) {
											if(grilleRef[i][incrJ] == 2) {
												ok= false;
											}
											incrJ--;
										}
									}
									if(grilleRef[i+1][j-1] == 0) {
										incrI=i+1;
										incrJ=j-1;
										while(incrI < grilleRef.length && incrJ >= 0 && (grilleRef[incrI][incrJ] == 0 || grilleRef[incrI][incrJ] == 2) && ok) {
											if(grilleRef[incrI][incrJ] == 2) {
												ok= false;
											}
											incrI++;
											incrJ--;
										}
									}
									if(grilleRef[i+1][j] == 0) {
										incrI=i+1;
										while(incrI < grilleRef.length && (grilleRef[incrI][j]== 0 || grilleRef[incrI][j] == 2) && ok) {
											if(grilleRef[incrI][j] == 2) {
												ok= false;
											}
											incrI++;
										}
									}
									if(grilleRef[i-1][j+1] == 0) {
										incrI=i-1;
										incrJ=j+1;
										while(incrI >= 0 && incrJ < grilleRef[i].length && (grilleRef[incrI][incrJ] == 0 || grilleRef[incrI][incrJ] == 2) && ok) {
											if(grilleRef[incrI][incrJ] == 2) {
												ok= false;
											}
											incrI--;
											incrJ++;
										}
									}
									if(grilleRef[i][j+1] == 0) {
										incrJ=j+1;
										while(incrJ < grilleRef[i].length && (grilleRef[i][incrJ] == 0 || grilleRef[i][incrJ] == 2) && ok) {
											if(grilleRef[i][incrJ] == 2) {
												ok= false;
											}
											incrJ++;
										}
									}
									if(grilleRef[i+1][j+1] == 0) {
										incrI=i+1;
										incrJ=j+1;
										while(incrI < grilleRef.length && incrJ < grilleRef[i].length && (grilleRef[incrI][incrJ] == 0 || grilleRef[incrI][incrJ] == 2) && ok) {
											if(grilleRef[incrI][incrJ] == 2) {
												ok= false;
											}
											incrI++;
											incrJ++;
										}
									}
								}
							}
						}
						if(ok) {
							grille[i][j] = 2;
						}
					}else if(grilleRef[i][j] == 2) {
						int okInt = 0;
						boolean ok=false;
						boolean trouve=false;
						int incrI = 0;
						int incrJ = 0;
						if(i == 0) {
							if(j==0) {
								if(grilleRef[i][j+1] == 2) {
									okInt+=2;
								}else if(grilleRef[i][j+1] == 0) {
									incrJ=j+1;
									trouve=false;
									while(incrJ<grilleRef[i].length && (grilleRef[i][incrJ] == 0 || grilleRef[i][incrJ] == 2) && !trouve) {
										if(grilleRef[i][incrJ] == 2) {
											okInt+=2;
											trouve=true;
										}
										incrJ++;
									}
								}
								if(grilleRef[i+1][j+1] ==2) {
									okInt+=2;
								} else if(grilleRef[i+1][j+1] == 0) {
									incrI=i+1;
									incrJ=j+1;
									trouve=false;
									while(incrI < grilleRef.length && incrJ<grilleRef[i].length && (grilleRef[incrI][incrJ] == 0 || grilleRef[incrI][incrJ] == 2) && !trouve) {
										if(grilleRef[incrI][incrJ] == 2) {
											okInt+=2;
											trouve=true;
										}
										incrI++;
										incrJ++;
									}
								}
								if(grilleRef[i+1][j] ==2) {
									okInt+=2;
								}else if(grilleRef[i+1][j] == 0) {
									incrI=i+1;
									trouve=false;
									while(incrI < grilleRef.length && (grilleRef[incrI][j] == 0 || grilleRef[incrI][j] == 2) && !trouve) {
										if(grilleRef[incrI][j] == 2) {
											okInt+=2;
											trouve=true;
										}
										incrI++;
									}
								}	
								if(okInt>= 10) {
									ok = true;
								}
							}else if(j == grilleRef[i].length-1 ) {
								if(grilleRef[i+1][j] == 2) {
									okInt+=2;
								}else if(grilleRef[i+1][j] == 0) {
									incrI=i+1;
									trouve=false;
									while(incrI < grilleRef.length && (grilleRef[incrI][j] == 0 || grilleRef[incrI][j] == 2) && !trouve) {
										if(grilleRef[incrI][j] == 2) {
											okInt+=2;
											trouve=true;
										}
										incrI++;
									}
								}
								if(grilleRef[i][j-1] == 2) {
									okInt+=2;
								}else if(grilleRef[i][j-1] == 0) {
									incrJ=j-1;
									trouve=false;
									while(incrJ >= 0 && (grilleRef[i][incrJ] == 0 || grilleRef[i][incrJ] == 2) && !trouve) {
										if(grilleRef[i][incrJ] == 2) {
											okInt+=2;
											trouve=true;
										}
										incrJ--;
									}
								}
								if(grilleRef[i+1][j-1] == 2) {
									okInt+=2;
								}else if(grilleRef[i+1][j-1] == 0) {
									incrI=i+1;
									incrJ=j-1;
									trouve=false;
									while(incrI < grilleRef.length && incrJ>=0 && (grilleRef[incrI][incrJ] == 0 || grilleRef[incrI][incrJ] == 2) && !trouve) {
										if(grilleRef[incrI][incrJ] == 2) {
											okInt+=2;
											trouve=true;
										}
										incrI++;
										incrJ--;
									}
								}	
								if(okInt>= 10) {
									ok = true;
								}
							}else {
								if(grilleRef[i][j+1] == 2) {
									okInt+=2;
								}else if(grilleRef[i][j+1] == 0) {
									incrJ=j+1;
									trouve=false;
									while(incrJ<grille[i].length && (grilleRef[i][incrJ] == 0 || grilleRef[i][incrJ] == 2) && !trouve) {
										if(grilleRef[i][incrJ] == 2) {
											okInt+=2;
											trouve=true;
										}
										incrJ++;
									}
								}
								if(grilleRef[i+1][j+1] == 2) {
									okInt+=2;
								}else if(grilleRef[i+1][j+1] == 0) {
									incrI=i+1;
									incrJ=j+1;
									trouve=false;
									while(incrI < grilleRef.length && incrJ<grilleRef[i].length && (grilleRef[incrI][incrJ] == 0 || grilleRef[incrI][incrJ] == 2) && !trouve) {
										if(grilleRef[incrI][incrJ] == 2) {
											okInt+=2;
											trouve=true;
										}
										incrI++;
										incrJ++;
									}
								}
								if(grilleRef[i+1][j] == 2) {
									okInt+=2;
								}else if(grilleRef[i+1][j] == 0) {
									incrI=i+1;
									trouve=false;
									while(incrI < grilleRef.length && (grilleRef[incrI][j] == 0 || grilleRef[incrI][j] == 2) && !trouve) {
										if(grilleRef[incrI][j] == 2) {
											okInt+=2;
											trouve=true;
										}
										incrI++;
									}
								}
								if(grilleRef[i][j-1] == 2) {
									okInt+=2;
								}else if(grilleRef[i][j-1] == 0) {
									incrJ=j-1;
									trouve=false;
									while(incrJ >= 0 && (grilleRef[i][incrJ] == 0 || grilleRef[i][incrJ] == 2) && !trouve) {
										if(grilleRef[i][incrJ] == 2) {
											okInt+=2;
											trouve=true;
										}
										incrJ--;
									}
								}
								if(grilleRef[i+1][j-1] == 2) {
									okInt+=2;
								}else if(grilleRef[i+1][j-1] == 0) {
									incrI=i+1;
									incrJ=j-1;
									trouve=false;
									while(incrI < grilleRef.length && incrJ >= 0 && (grilleRef[incrI][incrJ] == 0 || grilleRef[incrI][incrJ] == 2) && !trouve) {
										if(grilleRef[incrI][incrJ] == 2) {
											okInt+=2;
											trouve=true;
										}
										incrI++;
										incrJ--;
									}
								}	
								if(okInt>= 10) {
									ok = true;
								}
							}
	
						}else if(i == grilleRef.length-1 ) {
							if(j == 0 ) {
								if(grilleRef[i][j+1]==2) {
									okInt+=2;
								}else if(grilleRef[i][j+1] == 0) {
									incrJ=j+1;
									trouve = false;
									while(incrJ<grilleRef[i].length && (grilleRef[i][incrJ] == 0 || grilleRef[i][incrJ] == 2) && !trouve) {
										if(grilleRef[i][incrJ] == 2) {
											okInt+=2;
											trouve=true;
										}
										incrJ++;
									}
								}
								if(grilleRef[i-1][j+1]==2) {
									okInt+=2;
								}else if(grilleRef[i-1][j+1] == 0) {
									incrI=i-1;
									incrJ=j+1;
									trouve=false;
									while(incrI >= 0 && incrJ<grilleRef[i].length && (grilleRef[incrI][incrJ] == 0 || grilleRef[incrI][incrJ] == 2) && !trouve) {
										if(grilleRef[incrI][incrJ] == 2) {
											okInt+=2;
											trouve= true;
										}
										incrI--;
										incrJ++;
									}
								}
								if(grilleRef[i-1][j] ==2) {
									okInt+=2;
								}else if(grilleRef[i-1][j] == 0) {
									incrI=i-1;
									trouve=false;
									while(incrI >= 0 && (grilleRef[incrI][j]== 0 || grilleRef[incrI][j] == 2) && !trouve) {
										if(grilleRef[incrI][j] == 2) {
											okInt+=2;
											trouve=true;
										}
										incrI--;
									}
								}
								if(okInt>= 10) {
									ok = true;
								}
							}else if(j == grilleRef[i].length-1 ) {
								if(grilleRef[i][j-1]==2) {
									okInt+=2;
								}else if(grilleRef[i][j-1] == 0) {
									incrJ=j-1;
									trouve=false;
									while(incrJ >= 0 && (grilleRef[i][incrJ] == 0 || grilleRef[i][incrJ] == 2) && !trouve) {
										if(grilleRef[i][incrJ] == 2) {
											okInt+=2;
											trouve=true;
										}
										incrJ--;
									}
								}
								if(grilleRef[i-1][j-1]==2) {
									okInt+=2;
								}else if(grilleRef[i-1][j-1] == 0) {
									incrI=i-1;
									incrJ=j-1;
									trouve = false;
									while(incrI >= 0 && incrJ >= 0 && (grilleRef[incrI][incrJ] == 0 || grilleRef[incrI][incrJ] == 2) && !trouve) {
										if(grilleRef[incrI][incrJ] == 2) {
											okInt+=2;
											trouve = true;
										}
										incrI--;
										incrJ--;
									}
								}
								if(grilleRef[i-1][j]==2) {
									okInt+=2;
								}else if(grilleRef[i-1][j] == 0) {
									incrI=i-1;
									trouve= false;
									while(incrI >= 0 && (grilleRef[incrI][j]== 0 || grilleRef[incrI][j] == 2) && !trouve) {
										if(grilleRef[incrI][j] == 2) {
											okInt+=2;
											trouve= true;
										}
										incrI--;
									}
								}
								if(okInt>= 10) {
									ok = true;
								}
							}else {
								if(grilleRef[i][j-1]==2) {
									okInt+=2;
								}else if(grilleRef[i][j-1] == 0) {
									incrJ=j-1;
									trouve = false;
									while(incrJ >= 0 && (grilleRef[i][incrJ] == 0 || grilleRef[i][incrJ] == 2) && !trouve) {
										if(grilleRef[i][incrJ] == 2) {
											okInt+=2;
											trouve=true;
										}
										incrJ--;
									}
								}
								if(grilleRef[i-1][j-1]==2) {
									okInt+=2;
								} else if(grilleRef[i-1][j-1] == 0) {
									incrI=i-1;
									incrJ=j-1;
									trouve = false;
									while(incrI >= 0 && incrJ >= 0 && (grilleRef[incrI][incrJ] == 0 || grilleRef[incrI][incrJ] == 2) && !trouve) {
										if(grilleRef[incrI][incrJ] == 2) {
											okInt+=2;
											trouve=true;
										}
										incrI--;
										incrJ--;
									}
								}
								if(grilleRef[i-1][j]==2) {
									okInt+=2;
								}else if(grilleRef[i-1][j] == 0) {
									incrI=i-1;
									trouve=false;
									while(incrI >= 0 && (grilleRef[incrI][j]== 0 || grilleRef[incrI][j] == 2) && !trouve) {
										if(grilleRef[incrI][j] == 2) {
											okInt+=2;
											trouve=true;
										}
										incrI--;
									}
								}
								if(grilleRef[i][j+1]==2) {
									okInt+=2;
								}else if(grilleRef[i][j+1] == 0) {
									incrJ=j+1;
									trouve = false;
									while(incrJ<grilleRef[i].length && (grilleRef[i][incrJ] == 0 || grilleRef[i][incrJ] == 2) && !trouve) {
										if(grilleRef[i][incrJ] == 2) {
											okInt+=2;
											trouve = true;
										}
										incrJ++;
									}
								}
								if(grilleRef[i-1][j+1]==2) {
									okInt+=2;
								}else if(grilleRef[i-1][j+1] == 0) {
									incrI=i-1;
									incrJ=j+1;
									trouve = false;
									while(incrI >= 0 && incrJ < grilleRef[i].length && (grilleRef[incrI][incrJ] == 0 || grilleRef[incrI][incrJ] == 2) && !trouve) {
										if(grilleRef[incrI][incrJ] == 2) {
											okInt+=2;
											trouve=true;
										}
										incrI--;
										incrJ++;
									}
								}
								if(okInt>= 10) {
									ok = true;
								}	
							}
						}else {
							if(j==0) {
								if(grilleRef[i-1][j] ==2) {
									okInt+=2;
								}else if(grilleRef[i-1][j] == 0) {
									incrI=i-1;
									trouve=false;
									while(incrI >= 0 && (grilleRef[incrI][j]== 0 || grilleRef[incrI][j] == 2) && !trouve) {
										if(grilleRef[incrI][j] == 2) {
											okInt+=2;
											trouve=true;
										}
										incrI--;
									}
								}
								if(grilleRef[i-1][j+1] ==2) {
									okInt+=2;
								}else if(grilleRef[i-1][j+1] == 0) {
									incrI=i-1;
									incrJ=j+1;
									trouve = false;
									while(incrI >= 0 && incrJ<grilleRef[i].length && (grilleRef[incrI][incrJ] == 0 || grilleRef[incrI][incrJ] == 2) && !trouve) {
										if(grilleRef[incrI][incrJ] == 2) {
											okInt+=2;
											trouve=true;
										}
										incrI--;
										incrJ++;
									}
								}
								if(grilleRef[i][j+1] ==2) {
									okInt+=2;
								}else if(grilleRef[i][j+1] == 0) {
									incrJ=j+1;
									trouve=false;
									while(incrJ<grilleRef[i].length && (grilleRef[i][incrJ] == 0 || grilleRef[i][incrJ] == 2) && !trouve) {
										if(grilleRef[i][incrJ] == 2) {
											okInt+=2;
											trouve=true;
										}
										incrJ++;
									}
								}
								if(grilleRef[i+1][j] ==2) {
									okInt+=2;
								}else if(grilleRef[i+1][j] == 0) {
									incrI=i+1;
									trouve=false;
									while(incrI < grilleRef.length && (grilleRef[incrI][j]== 0 || grilleRef[incrI][j] == 2) && !trouve) {
										if(grilleRef[incrI][j] == 2) {
											okInt+=2;
											trouve = true;
										}
										incrI++;
									}
								}
								if(grilleRef[i+1][j+1]==2) {
									okInt+=2;
								}else if(grilleRef[i+1][j+1] == 0) {
									incrI=i+1;
									incrJ=j+1;
									trouve=false;
									while(incrI < grilleRef.length && incrJ<grilleRef[i].length && (grilleRef[incrI][incrJ] == 0 || grilleRef[incrI][incrJ] == 2) && !trouve) {
										if(grilleRef[incrI][incrJ] == 2) {
											okInt+=2;
											trouve=true;
										}
										incrI++;
										incrJ++;
									}
								}
								if(okInt>= 10) {
									ok = true;
								}
							}else if(j == grilleRef[i].length-1 ) {
								if(grilleRef[i-1][j-1] ==2) {
									okInt+=2;
								}else if(grilleRef[i-1][j-1] == 0) {
									incrI=i-1;
									incrJ=j-1;
									trouve = false;
									while(incrI >= 0 && incrJ >= 0 && (grilleRef[incrI][incrJ] == 0 || grilleRef[incrI][incrJ] == 2) && !trouve) {
										if(grilleRef[incrI][incrJ] == 2) {
											okInt+=2;
											trouve=true;
										}
										incrI--;
										incrJ--;
									}
								}
								if(grilleRef[i-1][j] ==2) {
									okInt+=2;
								}else if(grilleRef[i-1][j] == 0) {
									incrI=i-1;
									trouve=false;
									while(incrI >= 0 && (grilleRef[incrI][j]== 0 || grilleRef[incrI][j] == 2) && !trouve) {
										if(grilleRef[incrI][j] == 2) {
											okInt+=2;
											trouve=true;
										}
										incrI--;
									}
								}
								if(grilleRef[i][j-1] ==2) {
									okInt+=2;
								}else if(grilleRef[i][j-1] == 0) {
									incrJ=j-1;
									trouve=false;
									while(incrJ >= 0 && (grilleRef[i][incrJ] == 0 || grilleRef[i][incrJ] == 2) && !trouve) {
										if(grilleRef[i][incrJ] == 2) {
											okInt+=2;
											trouve = true;
										}
										incrJ--;
									}
								}
								if(grilleRef[i+1][j-1] ==2) {
									okInt+=2;
								}else if(grilleRef[i+1][j-1] == 0) {
									incrI=i+1;
									incrJ=j-1;
									trouve=false;
									while(incrI < grilleRef.length && incrJ >= 0 && (grilleRef[incrI][incrJ] == 0 || grilleRef[incrI][incrJ] == 2) && !trouve) {
										if(grilleRef[incrI][incrJ] == 2) {
											okInt+=2;
											trouve=true;
										}
										incrI++;
										incrJ--;
									}
								}
								if(grilleRef[i+1][j]==2) {
									okInt+=2;
								}else if(grilleRef[i+1][j] == 0) {
									incrI=i+1;
									trouve=false;
									while(incrI < grilleRef.length && (grilleRef[incrI][j]== 0 || grilleRef[incrI][j] == 2) && !trouve) {
										if(grilleRef[incrI][j] == 2) {
											okInt+=2;
											trouve=true;
										}
										incrI++;
									}
								}
								if(okInt>= 10) {
									ok = true;
								}
							}else {
								if(grilleRef[i-1][j-1] ==2) {
									okInt+=2;
								}else if(grilleRef[i-1][j-1] == 0) {
									incrI=i-1;
									incrJ=j-1;
									trouve = false;
									while(incrI >= 0 && incrJ >= 0 && (grilleRef[incrI][incrJ] == 0 || grilleRef[incrI][incrJ] == 2) && !trouve) {
										if(grilleRef[incrI][incrJ] == 2) {
											okInt+=2;
											trouve=true;
										}
										incrI--;
										incrJ--;
									}
								}
								if(grilleRef[i-1][j] ==2) {
									okInt+=2;
								}else if(grilleRef[i-1][j] == 0) {
									incrI=i-1;
									trouve=false;
									while(incrI >= 0 && (grilleRef[incrI][j]== 0 || grilleRef[incrI][j] == 2) && !trouve) {
										if(grilleRef[incrI][j] == 2) {
											okInt+=2;
											trouve=true;
										}
										incrI--;
									}
								}
								if(grilleRef[i-1][j+1] ==2) {
									okInt+=2;
								}else if(grilleRef[i-1][j+1] == 0) {
									incrI=i-1;
									incrJ=j+1;
									trouve = false;
									while(incrI >= 0 && incrJ<grilleRef[i].length && (grilleRef[incrI][incrJ] == 0 || grilleRef[incrI][incrJ] == 2) && !trouve) {
										if(grilleRef[incrI][incrJ] == 2) {
											okInt+=2;
											trouve=true;
										}
										incrI--;
										incrJ++;
									}
								}
								if(grilleRef[i][j-1] ==2) {
									okInt+=2;
								}else if(grilleRef[i][j-1] == 0) {
									incrJ=j-1;
									trouve=false;
									while(incrJ >= 0 && (grilleRef[i][incrJ] == 0 || grilleRef[i][incrJ] == 2) && !trouve) {
										if(grilleRef[i][incrJ] == 2) {
											okInt+=2;
											trouve = true;
										}
										incrJ--;
									}
								}
								if(grilleRef[i][j+1] ==2) {
									okInt+=2;
								}else if(grilleRef[i][j+1] == 0) {
									incrJ=j+1;
									trouve=false;
									while(incrJ<grilleRef[i].length && (grilleRef[i][incrJ] == 0 || grilleRef[i][incrJ] == 2) && !trouve) {
										if(grilleRef[i][incrJ] == 2) {
											okInt+=2;
											trouve=true;
										}
										incrJ++;
									}
								}
								if(grilleRef[i+1][j-1] ==2) {
									okInt+=2;
								}else if(grilleRef[i+1][j-1] == 0) {
									incrI=i+1;
									incrJ=j-1;
									trouve=false;
									while(incrI < grilleRef.length && incrJ >= 0 && (grilleRef[incrI][incrJ] == 0 || grilleRef[incrI][incrJ] == 2) && !trouve) {
										if(grilleRef[incrI][incrJ] == 2) {
											okInt+=2;
											trouve=true;
										}
										incrI++;
										incrJ--;
									}
								}
								if(grilleRef[i+1][j]==2) {
									okInt+=2;
								}else if(grilleRef[i+1][j] == 0) {
									incrI=i+1;
									trouve=false;
									while(incrI < grilleRef.length && (grilleRef[incrI][j]== 0 || grilleRef[incrI][j] == 2) && !trouve) {
										if(grilleRef[incrI][j] == 2) {
											okInt+=2;
											trouve=true;
										}
										incrI++;
									}
								}
								if(grilleRef[i+1][j+1]==2) {
									okInt+=2;
								}else if(grilleRef[i+1][j+1] == 0) {
									incrI=i+1;
									incrJ=j+1;
									trouve=false;
									while(incrI < grilleRef.length && incrJ<grilleRef[i].length && (grilleRef[incrI][incrJ] == 0 || grilleRef[incrI][incrJ] == 2) && !trouve) {
										if(grilleRef[incrI][incrJ] == 2) {
											okInt+=2;
											trouve=true;
										}
										incrI++;
										incrJ++;
									}
								}
								if(okInt>= 10) {
									ok = true;
								}
							}
						}
						if(ok) {
							grille[i][j] = 1;
						}
					}
				}
			}
			
			boolean egal = true;
		    for (int i = 0; i < grilleRef.length; i++) {
				for(int j=0;j< grilleRef[i].length;j++) {
			        if(grilleRef[i][j] != grille[i][j]) {
			        	egal = false;
			        }
		        }
		    }
		    
		    if(egal) {
		    	nonEgal = false;
		    }
		    
		    
		    
			for(int i=0;i< grille.length;i++) {
				for(int j=0;j< grille[i].length;j++) {
					grilleRef[i][j] = grille[i][j];
					if(grille[i][j]== 2) {
						retour++;
					}
				}
			}
			
		}
		
		return retour;
	}
}