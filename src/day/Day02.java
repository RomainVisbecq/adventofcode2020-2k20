package day;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Day02 {

	public static void main(String[] args) throws FileNotFoundException {
		
		Scanner sc = new Scanner(new File("src/sources/day02.txt"));
		
		ArrayList<String> totalPassword = new ArrayList<String>();
		
		while(sc.hasNextLine()){
			totalPassword.add(sc.nextLine());
		}
		
		System.out.println(partOne(totalPassword));
		System.out.println(partTwo(totalPassword));

	}
	
	static int partOne(ArrayList<String> totalPassword){
		int passOK =0;
		
		for(String ligne : totalPassword){
			String[] result = ligne.split(" ");
			int lettre = 0;
	        for (int i = 0; i < result[2].length(); i++) {
				if(result[2].charAt(i) == result[1].charAt(0)){
					lettre++;
				}
			}
	        String[] borne = result[0].split("-");
	        if(Integer.parseInt(borne[0])<=lettre  && lettre <=Integer.parseInt(borne[1])){
	        	passOK++;
	        }
		}
		return passOK;
	}
	
	static int partTwo(ArrayList<String> totalPassword){
		int passOK =0;
		
		for(String ligne : totalPassword){
			String[] result = ligne.split(" ");
			boolean uneSeuleLettre = false;
	        String[] borne = result[0].split("-");
	        if(result[2].charAt(Integer.parseInt(borne[0])-1) == result[1].charAt(0)){
	        	uneSeuleLettre = true;
	        }
	        
	        if(Integer.parseInt(borne[1])-1 < result[2].length() && result[2].charAt(Integer.parseInt(borne[1])-1) == result[1].charAt(0)){
	        	if(!uneSeuleLettre){
		        	uneSeuleLettre = true;
	        	}else{
	        		uneSeuleLettre = false;
	        	}
	        }
	        
	        if(uneSeuleLettre){
	        	passOK++;
	        }
		}
		return passOK;
	}

}
