package day;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class day16 {
	public static void main(String[] args) throws FileNotFoundException {
		
		
		Scanner sc = new Scanner(new File("src/sources/day16.txt"));
		ArrayList<String> champs = new ArrayList<String>();
		String ticket = new String();
		ArrayList<String> otherTicket = new ArrayList<String>();

		int section =0;
		while(sc.hasNextLine()){
			String insert =sc.nextLine();
			if(insert.trim().equals("")){
				section++;
			}else{
				if(section == 0){
					champs.add(insert);
				}else if (section == 1){
					if(!insert.equals("your tickets:")){
						ticket =insert;
					}
				}else{
					if(!insert.equals("nearby tickets:")){
						otherTicket.add(insert);
					}
				}
		}
			
		}
		
		System.out.println(partOne_Two(champs,ticket,otherTicket));

	}
	
	static long partOne_Two(ArrayList<String> champs,String ticket,ArrayList<String> otherTicket){
		long retour = 0;
		
		Map<String,Set<Integer>> mapChamps = new  HashMap<String,Set<Integer>>();
		Map<String,Set<Integer>> mapChampsRemove = new  HashMap<String,Set<Integer>>();
		
		for(String champ : champs){
			String[] champ2 = champ.split(": ");
			mapChamps.put(champ2[0],new HashSet<Integer>());
			mapChampsRemove.put(champ2[0],new HashSet<Integer>());
		}		
		
		ArrayList<String> removeTicket = new ArrayList<String>();
		for(String other :otherTicket){
			String[] alone = other.split(",");
			for(String number : alone){
				boolean dedans = false;
				for(String champ : champs){
					String[] bSepare = champ.split(": ");
					String[] bSepare2 = bSepare[1].split(" or ");
					String[] borne1 = bSepare2[0].split("-");
					String[] borne2 = bSepare2[1].split("-");
					if((Integer.valueOf(number)>= Integer.valueOf(borne1[0]) &&
							Integer.valueOf(number)<= Integer.valueOf(borne1[1])) || 
							(Integer.valueOf(number)>= Integer.valueOf(borne2[0]) &&
							Integer.valueOf(number)<= Integer.valueOf(borne2[1]))){
						dedans = true;
					}
				}
				if(!dedans){
					retour+= Integer.valueOf(number);
					removeTicket.add(other);
				}
			}
		}
		
		otherTicket.removeAll(removeTicket);

		for(String other :otherTicket){
			String[] alone = other.split(",");
			int nbBorne = 0;
			for(String number : alone){
				for(String champ : champs){
					String[] bSepare = champ.split(": ");
					String[] bSepare2 = bSepare[1].split(" or ");
					String[] borne1 = bSepare2[0].split("-");
					String[] borne2 = bSepare2[1].split("-");
					if((Integer.valueOf(number)>= Integer.valueOf(borne1[0]) &&
							Integer.valueOf(number)<= Integer.valueOf(borne1[1])) || 
							(Integer.valueOf(number)>= Integer.valueOf(borne2[0]) &&
							Integer.valueOf(number)<= Integer.valueOf(borne2[1]))){
						if(!mapChampsRemove.get(bSepare[0]).contains(nbBorne)){
							mapChamps.get(bSepare[0]).add(nbBorne);	
						}else{
							mapChamps.get(bSepare[0]).remove(nbBorne);
						}
					}else{
						mapChampsRemove.get(bSepare[0]).add(nbBorne);
						if(mapChamps.get(bSepare[0]).contains(nbBorne)){
							mapChamps.get(bSepare[0]).remove(nbBorne);
						}
					}
				}
				nbBorne++;
			}
		}
		
		boolean nonSeule = false;
		while(!nonSeule){
			ArrayList<Integer> champsRemove = new ArrayList<Integer>();
			int countChamps = 0;

			for(String champss : champs){
				String[] champ = champss.split(": ");
				if(mapChamps.get(champ[0]).size()==1){
					ArrayList<Integer> champC = new ArrayList<Integer>(mapChamps.get(champ[0]));
					champsRemove.add(champC.get(0));
					countChamps++;
				}
			}
			
			if(champs.size() == countChamps){
				nonSeule = true;
			}else{
		        for (Map.Entry<String,Set<Integer>> entry : mapChamps.entrySet()){
		        	if(entry.getValue().size()>1){
			        	for(Integer rem : champsRemove){
				        	entry.getValue().remove(rem);
			        	}	
		        	}
		        }	
			}
		}
		
		System.out.println(retour);
		
		long retour2 = 1;
		String[] ticketCoupe = ticket.split(",");
        for (Map.Entry<String,Set<Integer>> entry : mapChamps.entrySet()){
			if(entry.getKey().contains("departure")){
				ArrayList<Integer> tick = new ArrayList<Integer>(entry.getValue());
				retour2 = retour2 * Long.valueOf(ticketCoupe[tick.get(0)]);
			}
		}
		
		return retour2;
		
	}
}