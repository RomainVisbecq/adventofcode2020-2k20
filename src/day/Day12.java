package day;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Day12 {
	
public static void main(String[] args) throws FileNotFoundException {
			
	Scanner sc = new Scanner(new File("src/sources/day12.txt"));
	ArrayList<String> def = new ArrayList<String>();
	
	while(sc.hasNextLine()){
		String insert =sc.nextLine();
		def.add(insert);
	}
	
	System.out.println(partOne(def));
	System.out.println(partTwo(def));
	
}

private static int partOne(ArrayList<String> def) {

	Coordonates bateau = new Coordonates();
	//E = 0
	//S = 1
	//W = 2
	//N = 3
	
    for(String direction : def) {
    	
    	String aSuivre = null;
    	
    	if(direction.charAt(0) == 'F') {
    		aSuivre = bateau.axe+direction.substring(1);
    		if(aSuivre.charAt(0) == '0') {
    			aSuivre = 'E'+ aSuivre.substring(1);
    		}else if(aSuivre.charAt(0) == '1') {
    			aSuivre = 'S'+ aSuivre.substring(1);
    		}else if(aSuivre.charAt(0) == '2') {
    			aSuivre = 'W'+ aSuivre.substring(1);
    		}else {
    			aSuivre = 'N'+ aSuivre.substring(1);
    		}
    	}else if(direction.charAt(0) == 'L') {
    		int tourne = Integer.valueOf(direction.substring(1))/90;
    		bateau.axe-= tourne;
    		int nouveauD = bateau.axe%4;
    		if(nouveauD < 0) {
    			 nouveauD+=4;
    		}
    		bateau.axe = nouveauD;
    	}else if(direction.charAt(0) == 'R') {
    		int tourne = Integer.valueOf(direction.substring(1))/90;
    		bateau.axe+= tourne;
    		bateau.axe = bateau.axe%4;
    	}
    	
    	if(aSuivre==null) {
    		aSuivre = direction;
    	}

    	switch(aSuivre.charAt(0)) {
    	case 'N': 
    		bateau.vert += Integer.valueOf(aSuivre.substring(1));
    		break;
    	case 'S': 
    		bateau.vert -= Integer.valueOf(aSuivre.substring(1));
    		break;
    	case 'E': 
    		bateau.hori += Integer.valueOf(aSuivre.substring(1));
    		break;
    	case 'W': 
    		bateau.hori -= Integer.valueOf(aSuivre.substring(1));
    	default: break;
    	}
    	
    	bateau.manhattan= Math.abs(bateau.vert) + Math.abs(bateau.hori);	
    }

    	

	return bateau.manhattan;
}

private static int partTwo(ArrayList<String> def) {

	Coordonates bateau = new Coordonates();
	Coordonates waypoints = new Coordonates();
	waypoints.hori = 10;
	waypoints.vert = 1;
	//E = 0
	//S = 1
	//W = 2
	//N = 3
	
    for(String direction : def) {
    	boolean deplaceNavire = false;    	
    	if(direction.charAt(0) == 'F') {
			bateau.vert += Integer.valueOf(direction.substring(1))*waypoints.vert;
			bateau.hori += Integer.valueOf(direction.substring(1))*waypoints.hori;
    	}else if(direction.charAt(0) == 'L') {
    		int tourne = Integer.valueOf(direction.substring(1))/90;
    		int ancien = waypoints.axe;
    		waypoints.axe-= tourne;
    		int nouveauD = waypoints.axe%4;
    		if(nouveauD < 0) {
    			 nouveauD+=4;
    		}
    		waypoints.axe = nouveauD;
    		if(tourne==2) {
    			waypoints.vert = -waypoints.vert;
    			waypoints.hori = -waypoints.hori;
    		}else if(tourne==1) {
    			int temp = waypoints.vert;
    			waypoints.vert = waypoints.hori;
    			waypoints.hori = -temp;
    		}else if(tourne==3) {
    			int temp = waypoints.vert;
    			waypoints.vert = -waypoints.hori;
    			waypoints.hori = temp;
    		}
    	}else if(direction.charAt(0) == 'R') {
    		int tourne = Integer.valueOf(direction.substring(1))/90;
    		int ancien = waypoints.axe;
    		waypoints.axe+= tourne;
    		waypoints.axe = waypoints.axe%4;
    		if(tourne==2) {
    			waypoints.vert = -waypoints.vert;
    			waypoints.hori = -waypoints.hori;
    		}else if(tourne==1) {
    			int temp = waypoints.vert;
    			waypoints.vert = -waypoints.hori;
    			waypoints.hori = temp;
    		}else if(tourne==3) {
    			int temp = waypoints.vert;
    			waypoints.vert = waypoints.hori;
    			waypoints.hori = -temp;
    		}
    	}else {
        	switch(direction.charAt(0)) {
        	case 'N': 
        		waypoints.vert += Integer.valueOf(direction.substring(1));
        		break;
        	case 'S': 
        		waypoints.vert -= Integer.valueOf(direction.substring(1));
        		break;
        	case 'E': 
        		waypoints.hori += Integer.valueOf(direction.substring(1));
        		break;
        	case 'W': 
        		waypoints.hori -= Integer.valueOf(direction.substring(1));
        	default: break;
        	}	
    	}
    	
    	bateau.manhattan= Math.abs(bateau.vert) + Math.abs(bateau.hori);	
    }

    	

	return bateau.manhattan;
}

}

class Coordonates{
	int manhattan = 0;
	int vert =0;
	int hori = 0;
	int axe = 0;
}

